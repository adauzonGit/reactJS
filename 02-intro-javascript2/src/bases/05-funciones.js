



const saluda = function (nombre) {
    return `Hola ${nombre}`;
}

const saluda2 = (nombre) => {
    return `Hola ${nombre}`;
}

const saluda3 = (nombre) => `hola ${nombre}`;

console.log(saluda("Goku"));
console.log(saluda2("Goku"));
console.log(saluda3("Goku"));


const getUser = () => {
    return {
        uid: "DADAS",
        userName: "El_dodo"
    }

}
const getUser2 = () =>
    ({
        uid: "DADAS",
        userName: "El_dodo"
    })



console.log(getUser())

console.log(getUser2())

function getUsuarioActivo(nombre) {
    return {
        uid: "abc567",
        userName: nombre,
    }
}

const getUsuarioActivo2 = (nombre) => ({ uid: "abc567", userName: nombre });

console.log(getUsuarioActivo2("angel"))