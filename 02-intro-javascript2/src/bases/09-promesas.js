import { getHeroeById, getHeroesByOwner } from "./bases/08-imports-ex";



// const promesa = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         // resolve("2 segundos despues");
//         // console.log('2 segundos despues')
//         const heroe = getHeroeById(2);
//         resolve(heroe);
//         // reject("no se pudo resolvers")
//     }, 1000);
// });

// promesa.then(a => {
//     console.log(a);
// }).catch(err => {
//     console.log(err);
// }).finally(() => {
//     console.log("finalizo")
// })


const getHeroesByIdAsync = (id) => {
    const promesa = new Promise((resolve, reject) => {
        console.log("Hola");
        setTimeout(() => {
            // resolve("2 segundos despues");
            // console.log('2 segundos despues')
            const heroe = getHeroeById(id);
            heroe ? resolve(heroe) : reject("no se encontro");
            resolve(heroe);
            // reject("no se pudo resolvers")
        }, 1000);
    });
    return promesa;
}

getHeroesByIdAsync(4).then(console.log).catch(err => {
    console.log(err);
})
