


const personajes = ["G", "V", "T"];

console.log(personajes[0]);
console.log(personajes[1]);
console.log(personajes[2]);


const [, p2, p3] = personajes;


console.log(p2);
console.log(p3);


const retornaArreglo = () => {
    return ['Abc', 123];
}

const [texto, numero] = retornaArreglo();

console.log(texto);
console.log(numero);

const useStage = (valor) => {
    return [valor, () => console.log("Hola mundo")];
}

const [nombre,setNombre] = useStage("Angel");

console.log(nombre)

setNombre()