

//desestructuracion

const persona = {
    nombre: 'Tony',
    edad: 45,
    clave: 'iron man',

}


console.log(persona.nombre);
console.log(persona.edad);
console.log(persona.clave);

const { nombre, edad, clave } = persona;

console.log(nombre)
console.log(edad)
console.log(clave)


const useContext = ({ nombre, edad, clave }) => {


    console.log(nombre)
    console.log(edad)
    console.log(clave)

    return {
        nombreClave: clave,
        anios: edad,
        latlng: {
            lat: 3,
            long: 4
        }
    }
}

const aveng = useContext(persona);
const { nombreClave, anios, latlng: { lat, long } } = aveng;
console.log(nombreClave, anios);
