

const { heroes } = require("../data/heroes");




export const getHeroesByPublisher = (publisher) => {
    
    const heroePublishers = heroes.filter(heroe => heroe.publisher === publisher);
    if(heroePublishers.length === 0){
        throw new Error(`Publisher ${publisher} no validos`)
    }
    return heroePublishers;
}