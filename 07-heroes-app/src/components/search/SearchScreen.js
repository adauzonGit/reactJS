import React, { useMemo } from 'react'
import { heroes } from '../../data/heroes'
import { HeroCard } from '../heroes/HeroCard';
import { useForm } from '../../hooks/useForm';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string'
import { getHeroesByName } from '../../selectors/getHeroesByName';
export const SearchScreen = ({ history }) => {


    const location = useLocation();
    const { q = '' } = queryString.parse(location.search);



    const [formState,
        handledInputChanged
    ] = useForm({ search: q })

    const { search } = formState;

    const heroesFiltered = useMemo(() => getHeroesByName(q), [q]);
    // const heroesFiltered = ;
    const handledSearch = (e) => {
        e.preventDefault();

        history.push(`?q=${search}`)
    }
    return (
        <div>
            <h1>Search Screen</h1>
            <hr />

            <div className="row">
                <div className="col-5">
                    <h4>Search form</h4>
                    <hr />
                    <form onSubmit={handledSearch}>
                        <input
                            type="input"
                            placeholder="Find your hero"
                            className="form-control"
                            name='search'
                            value={search}
                            onChange={handledInputChanged}
                        >
                        </input>
                        <button
                            className="btn btn-block btn-outline-primary"
                            type="submitF"
                        >
                            Search
                        </button>
                    </form>
                </div>
                <div className="col-7">

                    <h4>Results</h4>
                    <hr />
                    {
                        heroesFiltered.map(hero => (
                            <HeroCard key={hero.id} {...hero} />
                        ))
                    }
                </div>
            </div>
        </div>
    )
}
