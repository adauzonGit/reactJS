import React, { useContext } from 'react'
import { types } from '../../types/types';
import { AuthContext } from '../../auth/AutoContext';

export const LoginScreen = ({ history }) => {

    const { dispatch } = useContext(AuthContext);


    const handleLogin = () => {        
        dispatch({ type: types.login, payload: { name: "Angel Alberto", email: "itzagi93@gmail.com" } });
        let path = localStorage.getItem('pathName') || '/';

        history.replace(path);
    };
    return (
        <div className="container mt-5">
            <h1 >LoginScreen</h1>
            <hr />
            <button onClick={handleLogin} className="btn btn-primary">Login</button>
        </div>
    )
}
