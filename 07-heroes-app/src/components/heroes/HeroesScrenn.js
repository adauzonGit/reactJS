import React, { useMemo } from 'react'
import { useParams, Redirect } from 'react-router-dom'
import { getHeroeById } from '../../selectors/getHeroesById';

export const HeroesScrenn = ({ history }) => {
    const { heroeId } = useParams();
    const hero = useMemo(() => getHeroeById(heroeId), [heroeId]);

    if (!hero) {
        return <Redirect to="./" />;
    }
    const { id,
        superhero,
        publisher,
        alter_ego,
        first_appearance,
        characters,
    } = hero;

    const handleReturn = () => {
        if (history.length <= 2) {
            history.push('/');
        } else {
            history.goBack();
        }
    }
    return (
        <div className="row mt-5">
            <div className="col-4">
                <img key={id} alt={superhero} src={`../assets/heroes/${heroeId}.jpg`} className="img-thumbnail animate__animated animate__fadeInLefth"></img>

            </div>
            <div className="col-8">
                <h3>{superhero}</h3>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item"><b>Alter ego : </b> {alter_ego}</li>
                    <li className="list-group-item"><b>Publisher : </b> {publisher}</li>
                    <li className="list-group-item"><b>First appearence : </b> {first_appearance}</li>
                </ul>
                <h5>Characters</h5>
                <p>{characters}</p>
                <button
                    className="btn btn-outline-info"
                    onClick={handleReturn}
                >
                    Regresar
                </button>
            </div>
        </div >
    )
}
