import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types';
export const PublicRoute = ({
    isAuthenticated,
    component: Component,
    ...rest2
}) => {
    return (
        <Route
            {...rest2}
            component={
                (props) => (
                    !isAuthenticated ?
                        (<Component {...props} />) :
                        (<Redirect to='/' />)
                )
            }
        >

        </Route >
    )
}


PublicRoute.protoTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired

}