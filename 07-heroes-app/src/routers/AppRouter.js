import React, { useContext } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    // Route,
    // Link
} from "react-router-dom";



// import { Navbar } from '../components/ui/NavBar';
import { LoginScreen } from '../components/login/LoginScreen';
import { DashboardRouter } from './DashboardRouter';
import { PriveteRoute } from './PriveteRoute';
import { AuthContext } from '../auth/AutoContext';
import { PublicRoute } from './PublicRoute';
export const AppRouter = () => {
    const { user } = useContext(AuthContext)
    return (
        <Router>
            <div>
                <Switch>
                    <PublicRoute isAuthenticated={user.logged} exact path="/login" component={LoginScreen} />
                    <PriveteRoute isAuthenticated={user.logged} path="/" component={DashboardRouter} />
                </Switch>
            </div>
        </Router>
    )
}
