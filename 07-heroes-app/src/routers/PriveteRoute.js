import React from 'react'
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom'

export const PriveteRoute = ({
    isAuthenticated,
    component: Component,
    ...rest2
}) => {



    localStorage.setItem('pathName', rest2.location.pathname);

    return (
        <Route
            {...rest2}
            component={
                (props) => (
                    isAuthenticated ?
                        (<Component {...props} />) :
                        (<Redirect to='/login' />)
                )
            }
        >
        </Route>
    )
}

PriveteRoute.protoTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired

}
