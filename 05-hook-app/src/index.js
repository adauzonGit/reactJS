import React from 'react';
import ReactDOM from 'react-dom';
// import { CounterWithCustumHook } from './Components/01-useState/CounterWithCustumHook';
// import { SimpleForm } from './Components/02-useEffect/SimpleForm';
// import { HookApp } from './HookApp';
// import { CounterApp } from './Components/01-useState/CounterApp';
// import { FormWithCustumHook } from './Components/02-useEffect/FormWithCustumHook';
// import { MultipleCustomHooks } from './Components/03-example/MultipleCustomHooks';
// import { FocusScreen } from './Components/04-useRef/FocusScreen';
// import { RealExampleReft } from './Components/04-useRef/RealExampleReft';
// import { LayoutEffect } from './Components/05-useLayoutEffect/LayoutEffect';
// import { Memorize } from './Components/06-memos/Memorize';
// import { MemoHook } from './Components/06-memos/MemoHook';
// import { CallbackHook } from './Components/06-memos/CallbackHook';
// import { Padre } from './Components/07-tarea-memo/Padre';
// import { TodoApp } from './Components/08-useReducer/TodoApp';
import { MainApp } from './Components/09-useContext/MainApp';

ReactDOM.render(
  // <React.StrictMode>
  <MainApp />
  // </React.StrictMode>
  ,
  document.getElementById('root')
);

// import './Components/08-useReducer/intro-reducer'