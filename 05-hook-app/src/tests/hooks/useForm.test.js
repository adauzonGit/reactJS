const { renderHook, act } = require("@testing-library/react-hooks")
const { useForm } = require("../../hooks/useForm")



describe('Pruebas en useForm', () => {

    const initalForm = {
        name: 'Angel',
        email: 'itzagi93'
    }
    test('should regresar valores de formulario', () => {
        const { result } = renderHook(() => useForm(initalForm));
        const [formState, handledInputChanged, reset] = result.current;
        expect(initalForm).toEqual(formState);
    })

    test('should cambiar el valor del formulario', () => {
        const _name = "Angel Alberto";
        const { result } = renderHook(() => useForm(initalForm));
        const [, handledInputChanged, reset] = result.current;
        act(() => { handledInputChanged({ target: { name: "name", value: _name } }) })
        const [formState] = result.current;
        expect(formState.name).toBe(_name);
    })


    test('should de resetear', () => {
        const _name = "Angel Alberto";
        const { result } = renderHook(() => useForm(initalForm));
        const [, handledInputChanged, reset] = result.current;
        act(() => { handledInputChanged({ target: { name: "name", value: _name } }) })
        const [formState] = result.current;
        expect(formState.name).toBe(_name);
        act(() => { reset() });
        expect(result.current[0]).toEqual(initalForm);


    })




})
