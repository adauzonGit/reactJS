const { renderHook } = require("@testing-library/react-hooks")
const { useFetch } = require("../../hooks/useFetch")


describe('Pruebas en useFetch', () => {
    test('should  regresar data', () => {
        const { result } = renderHook(() => useFetch('https://www.breakingbadapi.com/api/quotes/1'));
        const { data, loading, error } = result.current;
        expect(data).toBe(null);
        expect(loading).toBe(true);
        expect(error).toBe(null);
    })


    test('should  tener info deseada', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useFetch('https://www.breakingbadapi.com/api/quotes/1'));
        await waitForNextUpdate({ timeout: 1000000 });
        const { data, loading, error } = result.current;
        console.log(data);
        expect(data.length).toBe(1);
        expect(loading).toBe(false);
        expect(error).toBe(null);
    })

    test('should manejar el error', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useFetch('https://reqres.in/apid/users?page=2'));
        await waitForNextUpdate({ timeout: 1000000 });
        const { data, loading, error } = result.current;
        console.log(data);
        expect(data).toBe(null);
        expect(loading).toBe(false);
        expect(error).toBe('No se cargo info');
    })

})
