import '@testing-library/jest-dom'
import { useCounter } from '../../hooks/useCounter'
import { renderHook, act } from '@testing-library/react-hooks'



describe('Pruebas en useCounter', () => {
    test('should retornar valores por defecto', () => {
        const { result } = renderHook(() => useCounter())
        expect(result.current.state).toBe(10);
        expect(typeof result.current.increment).toBe("function");
        expect(typeof result.current.decrement).toBe("function");
    });
    test('should retornar state de 100', () => {
        const { result } = renderHook(() => useCounter(100))
        expect(result.current.state).toBe(100);
    });

    test('should debe incrementar el counter en 1', () => {
        const { result } = renderHook(() => useCounter(100))
        const { increment } = result.current;
        act(() => increment(1));
        const { state } = result.current;
        expect(state).toBe(101);

    });

    test('should decrementar el counter en 1', () => {
        const { result } = renderHook(() => useCounter(100))
        const { decrement } = result.current;
        act(() => decrement(1));
        const { state } = result.current;
        expect(state).toBe(99);
    });

    test('should debe resetear a 100', () => {
        const { result } = renderHook(() => useCounter(100))
        const { decrement, reset } = result.current;
        act(() => {


            decrement(1);
            reset();

        });
        const { state } = result.current;
        expect(state).toBe(100);
    });

})
