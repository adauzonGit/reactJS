const { shallow } = require("enzyme")
import { HookApp } from "../HookApp";
import '@testing-library/jest-dom';
import React from 'react';

describe('Pruebas sobre HookApp', () => {
    test('should Renderizar de manera correct', () => {
        const wrapper = shallow(<HookApp></HookApp>)
        expect(wrapper).toMatchSnapshot();
    })
})
