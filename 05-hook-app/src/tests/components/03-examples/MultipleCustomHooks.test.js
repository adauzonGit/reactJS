import React from 'react';
import { shallow } from 'enzyme';
import { MultipleCustomHooks } from '../../../Components/03-example/MultipleCustomHooks';
import '@testing-library/jest-dom'
import { useFetch } from '../../../hooks/useFetch';
import { useCounter } from '../../../hooks/useCounter';

jest.mock('../../../hooks/useCounter');

jest.mock('../../../hooks/useFetch');


describe('Pruebas en MultipleCustomHooks', () => {

    useCounter.mockReturnValue({
        counter: 100,
        increment:()=>{}
    })


    test('should renderizar correctamente', () => {
        useFetch.mockReturnValue({
            data: null,
            loading: true,
            error: null
        })

        const wrapper = shallow(<MultipleCustomHooks />)
        expect(wrapper).toMatchSnapshot();
    });


    test('should mostrar información', () => {

        useFetch.mockReturnValue({
            data: [{
                author: "angel",
                quote: 'hola mundo'
            }],
            loading: false,
            error: null
        })
        const wrapper = shallow(<MultipleCustomHooks />)
        expect(wrapper.find('.alert').exists()).toBe(false);
        expect(wrapper.find('.mb-0').text()).toBe('hola mundo');
        expect(wrapper.find('footer').text().trim()).toBe('angel');


        // expect(wrapper).toMatchSnapshot();


    })




})  
