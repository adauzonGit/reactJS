import React from 'react';
import { shallow, mount } from 'enzyme';
import { HomeScreen } from '../../../Components/09-useContext/HomeScreen';
import '@testing-library/jest-dom'
import { UserContext } from '../../../Components/09-useContext/UserContext';

describe('Pruebas en homeScreen', () => {


    const user = {
        name: 'Angel',
        email: 'itzangel'
    }
    const wrapper = mount(
        <UserContext.Provider value={{ user }}>
            <HomeScreen />
        </UserContext.Provider>
    )
    test('should de renderizar correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    })




})
