import React from 'react';
import { mount } from 'enzyme';
import { UserContext } from '../../../Components/09-useContext/UserContext';
import { LoginScreen } from '../../../Components/09-useContext/LoginScreen';



describe('Pruebas sobre LoginScren', () => {

    const setuser = jest.fn();

    const user = {
        name: 'Angel',
        email: 'itzangel'
    }
    const wrapper = mount(
        <UserContext.Provider value={{ user, setuser }}>
            <LoginScreen />
        </UserContext.Provider>
    );
    //  id: "1234a", name: "Angel Alberto" }
    test('should mostrarse correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    })

    test('should de ejecutar el setUser con el argumento esperado', () => {

        wrapper.find('button').simulate('click');

    })

    test('should de ejecutar setuser con el argumento esperado', () => {
        wrapper.find('button').simulate('click');
        expect(setuser).toBeCalledWith({ id: "1234a", name: "Angel Alberto" });

    })


})
