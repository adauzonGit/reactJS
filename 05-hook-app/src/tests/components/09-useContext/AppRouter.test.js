import React from 'react';
import { mount } from 'enzyme';
import { AppRouter } from '../../../Components/09-useContext/AppRouter';
import { UserContext } from '../../../Components/09-useContext/UserContext';


describe('Pruebas en AppRouter', () => {


    const user = {
        id: 2232,
        name: 'Angel Alberto',
        email: 'itzagi93@gmail.com'
    }
    const wrapper = mount(
        <UserContext.Provider value={{
            user

        }}>
            <AppRouter />
        </UserContext.Provider>
    );


    test('should  de renderizar correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    })

})
