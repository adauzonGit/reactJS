import React from 'react';
import { shallow } from 'enzyme';
import { RealExampleReft } from '../../../Components/04-useRef/RealExampleReft';
import '@testing-library/jest-dom'



describe('Pruebas en RealExampleRef', () => {
    test('should Renderizar', () => {
        const wrapper = shallow(<RealExampleReft />);
        expect(wrapper).toMatchSnapshot();
    })

    test('should mostrar componente <MultipleCustomHook/>', () => {
        const wrapper = shallow(<RealExampleReft />);
        wrapper.find("button").simulate("click");        
        expect((wrapper.find("MultipleCustomHooks").exists())).toBe(true);
    })


})
