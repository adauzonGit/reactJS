import React from 'react';
import { TodoApp } from '../../../Components/08-useReducer/TodoApp';
import { mount, shallow } from 'enzyme';
import { demoTodo } from '../../fixtures/demoTodos';
import { act } from '@testing-library/react';


describe('Pruebas en TodoApp', () => {

    Storage.prototype.setItem = jest.fn();

    const wrapper = shallow(<TodoApp />)

    test('should Renderizar correctamente', () => {
        // expect(wrapper).toMatchSnapshot();
    })


    test('should agregar un todo', () => {
        const wrapper = mount(<TodoApp />)
        act(() => {
            wrapper.find('TodoAdd').prop("handleAddTodo")(demoTodo[0]);
            // wrapper.find('TodoAdd').prop("handleAddTodo")(demoTodo[1]);
        });

        expect(wrapper.find('h1').text().trim()).toBe(`TodoApp (2)`)
        expect(localStorage.setItem).toHaveBeenCalledTimes(2);
        // expect(localStorage.setItem).toHaveBeenCalledWith({});

    })

    test('should Eliminar un todo', () => {
        wrapper.find("TodoAdd").prop('handleAddTodo')(demoTodo[0]);
        expect(wrapper.find('h1').text().trim()).toBe(`TodoApp (2)`)
        wrapper.find("TodoList").prop('handledBorrar')(demoTodo[0].id);
        expect(wrapper.find('h1').text().trim()).toBe(`TodoApp (1)`)

    })
    

})
