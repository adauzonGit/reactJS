import React from 'react';
import { shallow } from 'enzyme';
import { TodoListItem } from '../../../Components/08-useReducer/TodoListItem';
import { demoTodo } from '../../fixtures/demoTodos';
import '@testing-library/jest-dom'


describe('Pruebas en todoListItem', () => {

    const todos = demoTodo;
    const handledDelete = jest.fn();
    const handledComplete = jest.fn();
    const wrapper = shallow(<TodoListItem
        todo={todos[0]}
        handledBorrar={handledDelete}
        index={0}
        handledComplete={handledComplete}

    />);
    test('should Renderizar correctament', () => {
        expect(wrapper).toMatchSnapshot();
    })

    test('should llamar la función handledDelete', () => {
        wrapper.find("button").simulate("click");


        expect(handledDelete).toHaveBeenCalled();
    })

    test('should llamar la función handledComplete', () => {
        wrapper.find("p").simulate("click");



        expect(handledComplete).toHaveBeenCalledWith(1);
    })

    test('should mostrar el texto correctamente', () => {

        const textP = wrapper.find("p").text();

        expect(textP).toBe("1 " + todos[0].desc);
    })

    test('clase complete cuando done === true', () => {

        const todo = todos[0];
        todo.done = true;

        const wrapper = shallow(<TodoListItem todo={todo} />);

        expect(wrapper.find("p").hasClass("complete")).toBe(true);

    });


})
