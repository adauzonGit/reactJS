import React from 'react';
import { shallow } from 'enzyme';
import { TodoAdd } from '../../../Components/08-useReducer/TodoAdd';
import '@testing-library/jest-dom'

const handleAddTodo = jest.fn()

const wrapper = shallow(<TodoAdd
    handleAddTodo={handleAddTodo}
// key={new Date().getTime()}
/>);

describe('Pruebas sobre TodoAdd', () => {
    test('should renderizar correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    })

    test('should no debe de llamar handledAddTodo', () => {
        const formSubmit = wrapper.find("form").prop("onSubmit");



        expect(handleAddTodo).toHaveBeenCalledTimes(0)

    })
    test('should llamarse el handleAddTodo', () => {
        wrapper.find("input").simulate('change', {
            target: {
                value: 'Aprender Mongo',
                name: 'description'
            }
        })
        const formSubmit = wrapper.find("form").prop("onSubmit");
        formSubmit({
            preventDefault() { }
        })

        expect(handleAddTodo).toHaveBeenCalledTimes(1)
        expect(handleAddTodo).toHaveBeenCalledWith(expect.any(Object));
        expect(handleAddTodo).toHaveBeenCalledWith({
            desc: 'Aprender Mongo',
            done: false,
            id: expect.any(Number)

        });
        expect(wrapper.find('input').prop('value')).toBe('');
    })

 


})
