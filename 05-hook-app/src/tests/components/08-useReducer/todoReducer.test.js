const { todReducer } = require("../../../Components/08-useReducer/todoReducer");
const { demoTodo } = require("../../fixtures/demoTodos");


const demoTodos = demoTodo;

describe('Pruebas sobre todoReducer', () => {

    test('should de retornar el estado por defecto', () => {
        const state = todReducer(demoTodos, {});
        expect(state).toEqual(demoTodo);
    })

    test('should de agregar un todo', () => {
        const todo = {
            id: 4,
            desc: "Aprender testing",
            done: false,
        }
        const state = todReducer(demoTodos, { type: 'add', payload: todo });


        expect(state[state.length - 1]).toEqual(todo);
        expect(state).toEqual([...demoTodo, todo]);
    })

    test('should eliminar un todo', () => {
        const state = todReducer(demoTodos, { type: 'delete', payload: 2 });
        expect(state).toEqual(demoTodos.filter(todo=>todo.id!==2));
    })

    test('should modificar done', () => {
        const state = todReducer(demoTodos, { type: 'toggle', payload: 2 });
        expect(state.find(todo=>todo.id==2).done).toEqual(true);
        const _state = todReducer(state, { type: 'toggle', payload: 2 });
        expect(_state.find(todo=>todo.id==2).done).toEqual(false);
    })

})
