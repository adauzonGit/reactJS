import React from 'react';
import { shallow } from 'enzyme';
import { TodoList } from '../../../Components/08-useReducer/TodoList';
import { demoTodo } from '../../fixtures/demoTodos';


const handledComplete = jest.fn();
const handledBorrar = jest.fn();



const wrapper = shallow(<TodoList
    todos={demoTodo}
    handledComplete={handledComplete}
    handledBorrar={handledBorrar}
></TodoList>)


describe('Pruebas en TodoListItem', () => {
    test('should ', () => {
        expect(wrapper).toMatchSnapshot();

    })

    test('debe tener 2 TodoListItem', () => {
        expect(wrapper.find("TodoListItem").length).toBe(demoTodo.length);
        expect(wrapper.find("TodoListItem").get(0).props["handledComplete"]).toEqual(expect.any(Function));
    })
})

