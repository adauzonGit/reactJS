





import React, { useLayoutEffect, useRef, useState } from 'react'
import './style.css'
import { useFetch } from '../../hooks/useFetch'
import { useCounter } from '../../hooks/useCounter'
export const LayoutEffect = () => {

    const { state, increment } = useCounter(1);
    const { data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${state}`);
    const { quote } = !!data && data[0];
    const parrafo = useRef()

    const [boxSize, setboxSize] = useState({})

    useLayoutEffect(() => {

        setboxSize(parrafo.current.getBoundingClientRect())
        return () => {
            console.log("object")
        };
    }, [quote])
    return (
        <div>
            <h1>Layout - effects BreakingBad Quotes</h1>
            <hr />
            {/* {loading &&
                <div className="alert alert-info text-center">
                    Loading
            </div>} */}
            <pre>{JSON.stringify(boxSize, null, 3)}</pre>
            <blockquote className="blockquote text-right">
                <p ref={parrafo} className="mb-0">{quote}</p>
                {/* <footer className="blockquote-footer">{author} </footer> */}
            </blockquote>

            <button className="btn btn-primary" onClick={() => increment(1)}>Siguiente</button>
        </div>
    )
}
