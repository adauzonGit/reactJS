import React, { useState, useCallback } from 'react'
import '../05-useLayoutEffect/style.css'
import { ShowIncrement } from './ShowIncrement'


export const CallbackHook = () => {



    const [counter, setcounter] = useState(10)
    // const incrementer = () => {
    //     setcounter(counter + 1)
    // }
    const incrementer = useCallback((num) => {
        setcounter(c => c + num)
    }, [setcounter]);
    return (
        <div>
            <h1>UseCallaback Hook :{counter}</h1>
            <hr />

            <ShowIncrement increment={incrementer} />
            {/* <button className="btn btn-primary mt-3">+1</button> */}
        </div>
    )
}
