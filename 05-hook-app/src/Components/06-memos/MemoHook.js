import React, { useState, useMemo } from 'react'
import '../05-useLayoutEffect/style.css'
import { useCounter } from '../../hooks/useCounter'
import { Small } from './Small'
import { procesoPesado } from '../../helpers/procesoPesado'
export const MemoHook = () => {
    const { state, increment } = useCounter(5000)
    const [show, setShow] = useState(true);


    const memoProcessoPesado = useMemo(() => {
        procesoPesado(state);
    }, [state])



    return (
        <div>
            <h1>MemoHook</h1>
            <h3>Conter:
                <small> {state}</small>
                {/* <Small counter={state} /> */}
            </h3>
            <hr />
            <p>{memoProcessoPesado}</p>
            <button className="btn" onClick={() => { increment(1) }}>+1</button>
            <button className="btn btn-outline-primary ml-3" onClick={() => { setShow(!show) }}>Show/Hide {JSON.stringify(show)}</button>
        </div>
    )
}
