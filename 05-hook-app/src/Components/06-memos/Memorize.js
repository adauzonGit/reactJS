import React, { useState } from 'react'
import '../05-useLayoutEffect/style.css'
import { useCounter } from '../../hooks/useCounter'
import { Small } from './Small'
export const Memorize = () => {

    const { state, increment } = useCounter(10)

    const [show, setShow] = useState(true);
    return (
        <div>
            <h1>Conter:
                {/* <small> {state}</small> */}
                <Small counter={state} />
            </h1>
            <hr />
            <button className="btn" onClick={() => { increment(1) }}>+1</button>
            <button className="btn btn-outline-primary ml-3" onClick={() => { setShow(!show) }}>Show/Hide {JSON.stringify(show)}</button>
        </div>
    )
}
