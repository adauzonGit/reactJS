import React from 'react'
import './counter.css'
import { useCounter } from '../../hooks/useCounter'
export const CounterWithCustumHook = () => {


    const { state, increment, decrement, reset } = useCounter(100)

    return (
        <>
            <h1>Counter with hook: {state} </h1>
            <hr />
            <button className="btn btn-primary" onClick={() => increment(2)}>+1</button>
            <button className="btn btn-primary" onClick={reset}>reset</button>
            <button className="btn btn-primary" onClick={() => decrement(2)}>-1</button>
        </>
    )
}
