import React, { useState } from 'react'
import './counter.css'

export const CounterApp = () => {

    const [{ counter1, counter2 }, setCounter] = useState({
        counter1: 10,
        counter2: 10,
    });
    return (
        <>
            <h1>Counter {counter1}</h1>
            <h1>Counter {counter2}</h1>
            <hr />
            <button className="btn btn-primary" onClick={(() => {
                setCounter(state => {
                    return {
                        ...state,
                        counter2: counter2 + 1,
                    }
                })
            })}>+1</button>
        </>
    )
}
