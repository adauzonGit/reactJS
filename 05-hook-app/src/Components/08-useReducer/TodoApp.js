import React, { useReducer, useEffect } from 'react'
import './style.css'
import { todReducer } from './todoReducer';
import { TodoList } from './TodoList';
import { TodoAdd } from './TodoAdd';


const initialState = [{
    id: new Date().getTime(),
    desc: 'Aprender react',
    done: false
}];

const init = () => {
    return localStorage.getItem("todos") ?
        JSON.parse(localStorage.getItem("todos")) : initialState;
}

export const TodoApp = () => {

    const [todos, dispatch] = useReducer(todReducer, [], init)



    useEffect(() => {
        localStorage.setItem("todos", JSON.stringify(todos));
    }, [todos])


    const handledBorrar = (id) => {

        dispatch({
            type: "delete",
            payload: id
        })
    }

    const handledComplete = (id) => {
        dispatch({
            type: "toggle",
            payload: id
        })
    }

    const handledAdd = (todo) => {

        dispatch({
            type: "add",
            payload: todo

        })
    }

    return (
        <div>
            <h1>TodoApp ({todos.length}) </h1>
            <hr />
            <div className="row">
                <div className="col-7">
                    <TodoList todos={todos} handledBorrar={handledBorrar} handledComplete={handledComplete} />
                </div>
                <div className="col-5">
                    <TodoAdd handleAddTodo={handledAdd} />
                </div>
            </div>
        </div>
    )
}
