import React from 'react'

export const TodoListItem = ({ todo, handledBorrar, handledComplete, index: i }) => {
    return (
        <li className="list-group-item" key={todo.id}>
            <p className={`${todo.done && "complete"}`} onClick={() => handledComplete(todo.id)} >{i + 1} {todo.desc}</p>
            <button className="btn btn-danger" onClick={() => handledBorrar(todo.id)}>Borrar</button>
        </li>
    )
}
