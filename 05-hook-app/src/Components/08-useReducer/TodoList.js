import React from 'react'
import { TodoListItem } from './TodoListItem'

export const TodoList = ({ todos, handledComplete, handledBorrar }) => {
    return (
        <ol className="list-group list-group-flush">
            {todos.map((todo, i) => (                
                <TodoListItem key={todo.id} handledBorrar={handledBorrar} handledComplete={handledComplete} todo={todo} index={i} />
            ))}
        </ol>
    )
}
