


export const todReducer = (state = [], action) => {

    switch (action.type) {
        case 'add':
            return [...state, action.payload];
        case 'delete':
            return state.filter(sta => sta.id !== action.payload);
        case 'toggle-old':
            return state.map(todo => {
                if (action.payload === todo.id) {
                    return {
                        ...todo,
                        done: !todo.done
                    }
                }
                return todo;
            })
        case 'toggle-2':
            return state.map(todo => {
                return {
                    ...todo,
                    done: todo.id === action.payload ? !todo.done : todo.done
                }
            })
        case 'toggle':
            return state.map(todo => (todo.id === action.payload) ? { ...todo, done: !todo.done } : todo);
        default:
            return state;
    }
}