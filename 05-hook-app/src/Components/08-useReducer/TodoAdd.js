import React from 'react'
import { useForm } from '../../hooks/useForm';

export const TodoAdd = ({ handleAddTodo }) => {

    const [{ description }, handledInputChanged, reset] = useForm({ description: '' })
    const handledSubmit = (e) => {
        e.preventDefault();
        console.log("d",description)
        if (description.trim().length === 0) {
            return;
        }
        const newTodo = {
            id: new Date().getTime(),
            desc: description,
            done: false
        }
        handleAddTodo(newTodo);
        reset();
    }
    return (
        <>
            <h4>Agregar todo</h4>
            <hr />
            <form onSubmit={handledSubmit}>
                <input type="text"
                    name="description"
                    className="form-control"
                    placeholder="Aprender ..."
                    autoComplete="off"
                    onChange={handledInputChanged}
                    value={description}
                >
                </input>
                <button type="submit" className="btn btn-outline-primary btn-block mt-1">Agregar</button>
            </form>
        </>
    )
}
