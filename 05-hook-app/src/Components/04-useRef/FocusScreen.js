import React, { useRef } from 'react'
import '../02-useEffect/effects.css'
export const FocusScreen = () => {


    const inputRef = useRef();

    const handledClick = (e) => {
        inputRef.current.select();
    }



    return (
        <div>
            <h3>FocusScreen</h3>
            <hr />
            <input ref={inputRef} className="form-control" placeholder="su nombre"></input>
            <button className="btn btn-outline-primary mt-5" onClick={handledClick}>Focus</button>
        </div>
    )
}
