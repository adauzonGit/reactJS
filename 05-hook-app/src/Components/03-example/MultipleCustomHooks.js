import React from 'react'
import './custom.css'
import { useFetch } from '../../hooks/useFetch'
import { useCounter } from '../../hooks/useCounter'
export const MultipleCustomHooks = () => {


    // console.log(state);    
    // const { quote, author } = data ? data[0] : { quote: '', author: '' };
    const { state, increment } = useCounter(1);
    // console.log(state)
    const { loading, data, error } = useFetch(`https://www.breakingbadapi.com/api/quotes/${state}`);
    // console.log(data)
    const { quote, author } = !!data && data[0];
    return (
        <div>
            <h1>BreakingBad Quotes</h1>
            <hr />
            {loading &&
                <div className="alert alert-info text-center">
                    Loading
            </div>}
            {!loading && <blockquote className="blockquote text-right">
                <p className="mb-0">{quote}</p>
                <footer className="blockquote-footer">{author} </footer>
            </blockquote>}
            <button className="btn btn-primary" onClick={() => increment(1)}>Siguiente</button>
        </div>
    )
}
