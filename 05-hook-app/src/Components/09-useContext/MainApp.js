import React, { useState } from 'react'
import { AppRouter } from './AppRouter'
import { UserContext } from './UserContext'
export const MainApp = () => {
    // const user = {
    //     id: 1234,
    //     name: "angel",
    //     email: "angel@barsa05@hotmail.com"
    // }
    const [user, setuser] = useState({
        id: new Date().getTime()
    });
    return (
        // <div>
        //     <h1>MainScreen</h1>
        //     <hr />
        <UserContext.Provider value={{ user, setuser }}>
            <AppRouter></AppRouter>
        </UserContext.Provider>
        // </div>
    )
}
