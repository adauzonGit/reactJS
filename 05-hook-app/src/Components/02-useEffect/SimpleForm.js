import React, { useEffect, useState } from 'react'
import './effects.css';
import { Message } from './Message';
export const SimpleForm = () => {



    const [formState, setformState] = useState({
        name: '',
        email: ''
    })

    const { name, email } = formState;

    useEffect(() => {
        // console.log("hey");
    }, [])
    useEffect(() => {
        // console.log("hey form");
    }, [formState])
    useEffect(() => {
        // console.log("hey email");
    }, [email])

    const handledInputChanged = ({ target }) => {
        setformState({
            ...formState,
            [target.name]: target.value
        })
    }


    return (
        <>
            <h1>useEffetcs</h1>
            <hr />
            <div className="form-group">
                <input type="text" name="name"
                    className="form-control"
                    placeholder="Tu nombre"
                    autoComplete="off"
                    value={name}
                    onChange={handledInputChanged}
                ></input>
            </div>
            <div className="form-group">
                <input type="text" name="email"
                    className="form-control"
                    placeholder="Tu email"
                    autoComplete="off"
                    value={email}
                    onChange={handledInputChanged}
                ></input>
            </div>
            {name == "123" && <Message></Message>}

        </>
    )
}
