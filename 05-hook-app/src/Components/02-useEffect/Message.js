import React, { useEffect, useState } from 'react'

export const Message = () => {

    const [cords, setcords] = useState({ x: 0, y: 0 })
    const { x, y } = cords;
    useEffect(() => {

        const mouseMove = (e) => {
            const { x, y } = e;
            // console.log(x, y);
            // console.log(":D")
            setcords({ x, y });
        };
        window.addEventListener('mousemove', mouseMove);
        return () => {
            window.removeEventListener("mousemove", mouseMove);
        }
    }, []);

    return (
        <div>
            <h3>Coordenadas X: {x}  Y: {y}</h3>
        </div>
    )
}
