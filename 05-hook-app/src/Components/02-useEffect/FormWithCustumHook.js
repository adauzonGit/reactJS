import React, { useState, useEffect } from 'react'
import './effects.css';
import { useForm } from '../../hooks/useForm';
export const FormWithCustumHook = () => {



    const { formState, handledInputChanged } = useForm({
        name: '',
        email: '',
        password: ''
    });

    const { name, email, password } = formState;

    useEffect(() => {
        console.log("cambio email")
        // return () => {
        //     cleanup
        // }
    }, [email])

    const handledSubmit = (e) => {
        e.preventDefault();        
        console.log(formState);
    }

    return (
        <form onSubmit={handledSubmit}>
            <h1>FormWithCustumHook</h1>
            <hr />
            <div className="form-group">
                <input type="text" name="name"
                    className="form-control"
                    placeholder="Tu nombre"
                    autoComplete="off"
                    value={name}
                    onChange={handledInputChanged}
                ></input>
            </div>
            <div className="form-group">
                <input type="text" name="email"
                    className="form-control"
                    placeholder="Tu email"
                    autoComplete="off"
                    value={email}
                    onChange={handledInputChanged}
                ></input>
            </div>
            <div className="form-group">
                <input type="password" name="password"
                    className="form-control"
                    placeholder="****************"
                    autoComplete="off"
                    value={password}
                    onChange={handledInputChanged}
                ></input>
            </div>
            <button type="submit" className="btn btn-primary">Guardar</button>

        </form>
    )
}
