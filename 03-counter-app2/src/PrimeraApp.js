
import React from 'react';
import PropTypes from 'prop-types';

const PrimeraApp = ({ saludo, subtitulo }) => {


    // const saludo = 'Hola mundo const';
    const saludo2 = [1, 2, 3, 4];
    // console.log({ saludo, subtitulo="Soy un subtitulo" })

    return (
        <>
            <h1>{saludo}</h1>
            {/* <pre > {JSON.stinsaludo2}</pre > */}
            <p>{subtitulo}</p>
        </>
    )

}

PrimeraApp.propTypes = {
    saludo: PropTypes.string.isRequired

}
PrimeraApp.defaultProps = {
    subtitulo: "Soy un subtilo por defecto"
}

export default PrimeraApp;