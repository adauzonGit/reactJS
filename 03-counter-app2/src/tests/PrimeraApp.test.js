const { render } = require("@testing-library/react");
const { default: PrimeraApp } = require("../PrimeraApp");
import '@testing-library/jest-dom';
import React from 'react';
import { shallow } from 'enzyme';

describe('Pruebas de priera app', () => {
    // test('should render', () => {
    //     const saludo = 'Hola, soy goku';

    //     // const { getByText } = render(<PrimeraApp saludo="Hola, soy goku" />);
    //     const wrapper = render(<PrimeraApp saludo="Hola, soy goku" />);

    //     // expect(getByText(saludo)).toBeInTheDocument(true);
    //     expect(wrapper.getByText(saludo)).toBeInTheDocument(true);



    // })
    test('should renderizar correctamente', () => {
        const saludo = "hola soy goku";
        const wrapper = shallow(<PrimeraApp saludo={saludo} />);

        expect(wrapper).toMatchSnapshot();



    })
    test('should debe mostrar subtiulo correctamente', () => {
        const subtitulo = "Soy un sub";
        const saludo = "hola soy goku";
        const wrapper = shallow(<PrimeraApp
            saludo={saludo}
            subtitulo={subtitulo}
        />);

        const textoParrafo = wrapper.find("p").text();
        console.log(textoParrafo);


        expect(textoParrafo).toBe(subtitulo);

    })


})
