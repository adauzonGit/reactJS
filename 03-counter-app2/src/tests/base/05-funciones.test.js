const { getUser, getUsuarioActivo } = require("../../base-pruebas/05-funciones")




describe('Prueba funciones', () => {
    test('get user debe retornar un boje', () => {




        const user = {
            uid: 'ABC123',
            username: 'El_Papi1502'
        }
        const userTest = getUser();

        expect(user).toEqual(userTest);


    })
    test('get user Activo debe retornar un boje', () => {


        const nombre = "Angel";

        const user = {
            uid: 'ABC567',
            username: nombre
        }
        const userTest = getUsuarioActivo(nombre);

        expect(user).toEqual(userTest);


    })

})
