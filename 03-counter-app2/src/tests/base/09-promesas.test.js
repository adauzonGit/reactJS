import { getHeroeByIdAsync } from '../../base-pruebas/09-promesas';
import heroes from '../../data/heroes';



describe('Pruebas promesas', () => {
    test('should retornar un hereo async', (done) => {
        const id = 1;
        getHeroeByIdAsync(id).then(heroe => {
            console.log(heroe);
            expect(heroe).toBe(heroes[0]);
            done();
        });

    });

    test('should error cuando no existe', (done) => {
        const id = 44;
        getHeroeByIdAsync(id).then(a => { }).catch(b => {
            console.log(b);

            expect(b).toBe("No se pudo encontrar el héroe");
            done();
        })
    })


})





