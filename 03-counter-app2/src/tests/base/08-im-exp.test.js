const { getHeroeById,getHeroesByOwner } = require("../../base-pruebas/08-imp-exp");
const { default: heroes } = require("../../data/heroes");





describe('Pruebas 08', () => {
    test('Primer metodo funcion por id ', () => {
        const id = 1;
        const heroe = getHeroeById(id);
        const heroesData = heroes.find(a => a.id === id);
        console.log(heroe);

        expect(heroe).toEqual(heroesData);

    })
    test('Primer metodo funcion por id no existente', () => {
        const id = 20;
        const heroe = getHeroeById(id);
        const heroesData = heroes.find(a => a.id === id);
        console.log(heroe);

        expect(heroe).toEqual(undefined);

    })
    test('Primer metodo funcion por id no existente', () => {
        const id = 20;
        const marvel = getHeroesByOwner("Marvel");
        // const marvelData = heroes.filter(a => a.owner === "Marvel");
        

        expect(marvel.length).toBe(2);

    })
    test('Primer metodo funcion por id no existente', () => {
        const id = 20;
        const dc = getHeroesByOwner("DC");
        const DCData = heroes.filter(a => a.owner === "DC");
        

        expect(dc).toEqual(DCData);

    })

})











