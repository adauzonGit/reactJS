import '@testing-library/jest-dom';
import { getSaludo } from "../../base-pruebas/02-template-string";


describe('Pruebas templetes string', () => {


    test('Prueba1 metodo saludar', () => {
        const nombre = "Angel";

        const saludo = getSaludo(nombre);
        expect(saludo).toBe("Hola "+nombre)
    });

    test('Prueba1 metodo saludar 2', () => {
        const nombre = "Angel";
    
        const saludo = getSaludo();
        expect(saludo).toBe("Hola carlos")
    });

});
