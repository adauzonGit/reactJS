import '@testing-library/jest-dom';
import CounterApp from '../CounterApp';
import { shallow } from 'enzyme';
import React from 'react';



describe('pruebas counterApp', () => {

    let wrapper = shallow(<CounterApp></CounterApp>);

    beforeEach(() => {
        wrapper = shallow(<CounterApp></CounterApp>);
    })

    test('should renderizar de manera normal', () => {
        expect(wrapper).toMatchSnapshot();
    })

    test('should Mostrar 100', () => {
        const initCounter = 100;
        const wrapper = shallow(<CounterApp value={initCounter}></CounterApp>)
        const counterText = wrapper.find("h2").text();
        if (isNaN(counterText)) {
            throw "Fallo en el numero recibido " + counterText;
        }
        expect(Number(counterText)).toBe(initCounter);

    })


    test('should Debe incrementar', () => {
        // const btn1 = wrapper.find("button").at(0);
        // const btn2 = wrapper.find("button").at(1);
        // const btn3 = wrapper.find("button").at(2);   

        wrapper.find("button").at(0).simulate('click');
        const counterText = wrapper.find("h2").text();

        expect(Number(counterText)).toBe(11);
        // wrapper.find("button").at(1);
        // wrapper.find("button").at(2);


        // console.log(btn1, btn1.html())   
    })
    test('should Debe decrementar', () => {

        wrapper.find("button").at(2).simulate('click');
        wrapper.find("button").at(2).simulate('click');

        const counterText = wrapper.find("h2").text();
        expect(Number(counterText)).toBe(8);
    })

    test('should Debe reset', () => {

        wrapper.find("button").at(2).simulate('click');
        wrapper.find("button").at(1).simulate('click');

        const counterText = wrapper.find("h2").text();
        expect(Number(counterText)).toBe(10);
    })



})
