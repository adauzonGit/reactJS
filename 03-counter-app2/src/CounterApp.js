
import React, { useState } from 'react';
import PropTypes from 'prop-types';

const CounterApp = ({ value = 10 }) => {


    // const [state, setNombre] = useState("initialState");
    // setNombre("nuevo valor");
    // cambio por dos ddd
    const [counter, setCounter] = useState(value);


    const handelAdd = (e) => {
        setCounter(counter + 1)
    }
    const handelReset = (e) => {
        setCounter(value)
    }
    const handelSubstrac = (e) => {
        setCounter(counter - 1)
    }

    return <>
        <h1>CounterApp</h1>
        <h2>{counter}</h2>
        <button onClick={handelAdd}>+1</button>
        <button onClick={handelReset}>+Reset</button>
        <button onClick={handelSubstrac}>-1</button>
    </>
}


CounterApp.propTypes = {
    value: PropTypes.number

}

export default CounterApp;