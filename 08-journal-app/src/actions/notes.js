import { db } from "../firebase/firebase-config";
import { types } from "../types/types";
import { loadNotes } from "../helpers/loadNotes";
import Swal from "sweetalert2";
import { fileUpload } from "../helpers/fileUpload";


export const startNewNote = () => {

    return async (dispach, getState) => {
        const { uid } = getState().auth;
        const newNote = {
            title: '',
            body: '',
            date: new Date().getTime()
        }

        const docRef = await db.collection(`${uid}/journal/notes`).add(newNote);

        dispach(activeNote(docRef.id, newNote));
        dispach(addNewNote(docRef.id,newNote));
    }
}


export const activeNote = (id, note) => ({
    type: types.notesActive,
    payload: {
        id,
        ...note,
    }
})

export const addNewNote = (id, note) => ({
    type: types.notesAddNew,
    payload: {
        id,
        ...note,
    }
})
export const startSaveNote = (note) => {
    return async (dispach, getState) => {
        const { uid } = getState().auth;
        if (!note.urlImage) delete note.urlImage;
        const noteToFireStore = { ...note };
        delete noteToFireStore.id;
        await db.doc(`${uid}/journal/notes/${note.id}`).update(noteToFireStore);

        dispach(refresNote(note.id, note));
        Swal.fire('Saved', note.title, 'success')
    }
}
export const setNotes = (notes) => ({
    type: types.notesLoad,
    payload: notes
})

export const startLoadingNotes = (uid) => {
    return async (dispatch) => {
        const notes = await loadNotes(uid);
        dispatch(setNotes(notes));
    };
}

export const refresNote = (id, note) => ({
    type: types.notesUpdate,
    payload: {
        id, note
    }

})


export const startUploadImage = (file) => {
    return async (dispatch, getState) => {
        const { active: activeNote } = getState().notes
        Swal.fire({
            title: "Uploading.....",
            text: "Please wait",
            allowOutsideClick: false,
            onBeforeOpen: () => {
                Swal.showLoading();
            }
        })
        const url = await fileUpload(file);
        activeNote.urlImage = url;
        Swal.close();
        dispatch(startSaveNote({ ...activeNote }));
    }
}

export const startDeleting = (id) => {


    return async (dispach, getState) => {
        const uid = getState().auth.uid;
        await db.doc(`${uid}/journal/notes/${id}`).delete();
        dispach(deleteNote(id));

    }
}

export const deleteNote = (id) => ({
    type: types.notesDelete,
    payload: id
})


export const noteLogout = () => ({
    type: types.notesLogoutCleaning
})