import { types } from "../types/types"
import { firebase, googleAuthProvider } from '../firebase/firebase-config'
import { startLoading, endLoading } from "./ui"
import Swal from "sweetalert2"
import { noteLogout } from "./notes"



export const startLogout = () => {
    return async (dispatch) => {
        await firebase.auth().signOut();
        dispatch(logout());
        dispatch(noteLogout());
    }
}
export const logout = () => ({
    type: types.logout,

});


export const startLogingEmailPassword = (email, password) => {
    return async (dispatch) => {
        try {
            dispatch(startLoading());
            const { user } = await firebase.auth().signInWithEmailAndPassword(email, password);
            dispatch(login(user.uid, user.displayName));
            dispatch(endLoading());
        } catch (error) {
            dispatch(endLoading());
            console.log(error);
            Swal.fire('Fail', error.message,'error');
            
        }

    }
}

export const startRegisterWithEmailPasswordName = (email, password, name) => {
    return dispatch => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(async ({ user }) => {
                await user.updateProfile({ displayName: name });
                dispatch(login(user.uid, user.displayName));
            }).catch(error => {
                
                Swal.fire('Fail', error.message,'error');
            });
    }
}


export const startGoogleLogin = () => {
    return (dispatch) => {
        firebase.auth().signInWithPopup(googleAuthProvider)
            .then(({ user }) => {
                dispatch(login(user.uid, user.displayName))
            });
    }
}

export const login = (uid, displayName) => ({
    type: types.login,
    payload: {
        uid,
        displayName
    }
});
