import { types } from "../types/types";

/*
    {
        notes:[],
        active:null --- active{ id: 0dasdas , title:'', imageUrl:'',date:''}
    }
*/
const initialState = {
    notes: [],
    active: null
}
export const notesReducer = (state = initialState, action) => {
    switch (action.type) {

        case types.notesAddNew:
            return {
                ...state,
                notes: [...state.notes, { ...action.payload }]
            }

        case types.notesActive:
            return {
                ...state,
                active: {
                    ...action.payload
                }
            }
        case types.notesLoad:
            return {
                ...state,
                notes: [...action.payload]
            }
        case types.notesUpdate:
            return {
                ...state,
                notes: state.notes.map(nota => nota.id === action.payload.id ? action.payload.note : nota)
            }
        case types.notesDelete:
            return {
                ...state,
                active: null,
                notes: state.notes.filter(nota => nota.id !== action.payload)
            }
        case types.notesLogoutCleaning:
            return { ...initialState }
        default:
            return state;

    }
}