import React from 'react'
import { Link } from 'react-router-dom'
import { useForm } from '../../hooks/useForm'
import { useDispatch, useSelector } from 'react-redux'
import { startLogingEmailPassword, startGoogleLogin } from '../../actions/auth'

const initValue = {
    email: "test@gmail.com",
    password: "123456",
}

export const LoginScreen = () => {

    const [values, handleInputChange] = useForm(initValue);
    const { loading } = useSelector(state => state.ui);
    const { email, password } = values;
    const dispatch = useDispatch();
    const handleLogin = (e) => {
        e.preventDefault();
        dispatch(startLogingEmailPassword(email, password))
    }
    const handleGoogleLogin = (e) => {
        e.preventDefault();
        dispatch(startGoogleLogin())
    }
    return (
        <>
            <h3 className="auth__title">Login</h3>
            <form onSubmit={handleLogin} disabled={loading} className="animate__animated animate__fadeIn animate_faster">
                <input className="auth__input"
                    type="text"
                    placeholder="Email"
                    name="email"
                    autoComplete="off"
                    onChange={handleInputChange}
                    value={email}

                />
                <input className="auth__input"
                    type="password"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={handleInputChange}
                />
                <button className="btn btn-primary btn-block" disabled={loading} type="submit" >Login</button>

                <hr />
                <div className="auth__social-networks">
                    <p>Login with social networks</p>
                    <div className="google-btn" onClick={handleGoogleLogin}>
                        <div className="google-icon-wrapper">
                            <img className="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="google button" />
                        </div>
                        <p className="btn-text">
                            <b>Sign in with google</b>
                        </p>
                    </div>
                </div>
                <Link to='/auth/register' className="link"> Create new account</Link>
            </form>
        </>
    )
}
