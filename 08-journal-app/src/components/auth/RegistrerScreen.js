import React from 'react'
import { Link } from 'react-router-dom'
import { useForm } from '../../hooks/useForm'
import validator from 'validator';
import { useDispatch, useSelector } from 'react-redux';
import { setError, removeError } from '../../actions/ui';
import { startRegisterWithEmailPasswordName } from '../../actions/auth';

export const RegisterScreen = () => {


    const dispatch = useDispatch();
    const state = useSelector(state => state.ui);
    const { msgError } = state;
    const [formValues, handleInputChanged] = useForm({
        name: "test",
        email: "test@gmail.com",
        password: "123456",
        password2: "123456"

    });

    const { name, password, password2, email } = formValues;
    const handleRegister = (e) => {
        e.preventDefault();
        if (isFormValid()) {
            dispatch(startRegisterWithEmailPasswordName(email,password,name));
        }
    }
    const isFormValid = () => {
        if (name.trim().length === 0) {
            dispatch(setError('Name is required'));
            return false;
        } else if (!validator.isEmail(email)) {
            dispatch(setError('Email is required'));
            return false;
        } else if (password !== password2 || password.length < 5) {
            dispatch(setError('Password invalid'));
            return false;
        }
        dispatch(removeError());
        return true;
    }



    return (
        <>
            <h3 className="auth__title">Register</h3>
            <form onSubmit={handleRegister} className="animate__animated animate__fadeIn animate_faster">

                {
                    msgError &&
                    < div className="auth__alert-error">
                        {msgError}
                    </div>
                }

                <input className="auth__input"
                    type="text"
                    placeholder="Name"
                    name="name"
                    autoComplete="off"
                    value={name}
                    onChange={handleInputChanged}
                />
                <input className="auth__input"
                    type="text"
                    placeholder="Email"
                    name="email"
                    autoComplete="off"
                    value={email}
                    onChange={handleInputChanged}
                />
                <input className="auth__input"
                    type="password"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={handleInputChanged}
                />
                <input className="auth__input"
                    type="password"
                    placeholder="Confrim"
                    name="password2"
                    value={password2}
                    onChange={handleInputChanged}
                />
                <button className="btn btn-primary btn-block mb-5" type="submit" >Login</button>



                <Link to='/auth/login' className="link"> Already registered</Link>
            </form>
        </>
    )
}
