import React from 'react'
import { Sidebar } from './Sidebar'

import { NotesScreem } from '../notes/NotesScreem'
import { useSelector } from 'react-redux'
import { NothingSelected } from './NothingSelected'

export const JournalScreen = () => {
    const { active } = useSelector(state => state.notes);
    return (
        <div className="journal__main-content animate__animated animate__fadeIn animate_faster">
            <Sidebar />
            <main>
                {
                    (active) ? (<NotesScreem />) : (<NothingSelected />)
                }            
            </main>
        </div>
    )
}
