import React from 'react'
import moment from 'moment'
import { useDispatch } from 'react-redux';
import { activeNote } from '../../actions/notes';

export const JournalEntry = ({ body, date, id, title, urlImage }) => {

    // const { body, date, id, title } = data;
    const noteDate = moment(date);
    const dispatch = useDispatch();
    const handleClick = () => {
        dispatch(activeNote(id, { body, date, title, urlImage }));
    }
    return (
        <div className="journal__entry animate__animated animate__fadeIn animate_faster" onClick={handleClick}>
            {urlImage && <div className="journal__entry-picture"
                style={{
                    backgroundSize: '100% 100%',
                    // backgroundSize:'initial',
                    // backgroundRepeat:'no-repeat',
                    // width:'100%',
                    // height:'100%',
                    backgroundImage: `url(${urlImage})`,
                    // backgroundImage: 'url(https://www.freecodecamp.org/news/content/images/size/w2000/2020/02/Ekran-Resmi-2019-11-18-18.08.13.png)',

                }}
            ></div>
            }

            <div className="journal__entry-body">
                <p className="journal__entry-title">{title}</p>
                <p className="journal__entry-content">{body}</p>

            </div>
            <div className="journal__entry-date-box">
                <span>{noteDate.format('dddd')}</span>
                <h4>{noteDate.format('Do')}</h4>
            </div>

        </div>
    )
}
