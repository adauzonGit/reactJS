import React, { useEffect, useRef } from 'react'
import { NoteAppBar } from './NoteAppBar'
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from '../../hooks/useForm';
import { activeNote, startDeleting } from '../../actions/notes';

export const NotesScreem = () => {

    const dispatch = useDispatch();
    const { active: note } = useSelector(state => state.notes);

    const [values, handleInputChange, reset] = useForm(note);

    const { title: title_, body: body_ } = values;

    const activeId = useRef(note.id);
    useEffect(() => {
        if (note.id !== activeId.current) {
            reset(note);
            activeId.current = note.id;
        }
    }, [note, reset]);

    useEffect(() => {

        dispatch(activeNote(values.id, { ...values }));
    }, [values, dispatch])
    const handleDelete = () => {
        dispatch(startDeleting(note.id));
    }
    return (
        <div className="notes__main_content">
            <NoteAppBar />
            <div className="notes__content">
                <input
                    type="text"
                    placeholder="Some awesome title"
                    className="notes__title-input"
                    name="title"
                    value={title_}
                    onChange={handleInputChange}
                />
                <textarea
                    placeholder="What happened todo"
                    className="notes__textarea"
                    name="body"
                    value={body_}
                    onChange={handleInputChange}
                ></textarea>
                {note.urlImage && <div className="notes__image">
                    <img
                        src={note.urlImage}
                        alt="some"
                    />
                </div>}

            </div>
            <button className="btn btn-danger" onClick={handleDelete}>Delete</button>
        </div>
    )
}
