import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { startSaveNote, startUploadImage } from '../../actions/notes';

export const NoteAppBar = () => {
    const dispatch = useDispatch();
    const { active } = useSelector(state => state.notes);
    const { date } = active;
    const handleSaveClick = () => {
        dispatch(startSaveNote(active));
    }

    const handleUpdatePicture = () => {
        document.getElementById("fileSelector").click();

    }

    const handelFileChange = (e) => {
        const file = e.target.files[0];
        if (file) {
            dispatch(startUploadImage(file));
        }
    }

    return (
        <div className="notes__appbar">
            <span>{new Date(date).toLocaleString()}</span>
            <input id="fileSelector" type="file" style={{ display: 'none' }} onChange={handelFileChange}  ></input>
            <div>
                <button className="btn" onClick={handleUpdatePicture}>Picture</button>
                <button className="btn" onClick={handleSaveClick}>Save</button>
            </div>
        </div>
    )
}
