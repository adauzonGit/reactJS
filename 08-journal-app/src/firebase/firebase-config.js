import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'






const firebaseConfig = {
    apiKey: "AIzaSyBQJTOjlxjisrDkaLoSADvdlPveKUM8ZgE",
    authDomain: "react-pruebas-e77d4.firebaseapp.com",
    databaseURL: "https://react-pruebas-e77d4.firebaseio.com",
    projectId: "react-pruebas-e77d4",
    storageBucket: "react-pruebas-e77d4.appspot.com",
    messagingSenderId: "591735304890",
    appId: "1:591735304890:web:bffef9dfb2c7ed773c0883"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore();
  const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

  export {
      db,
      googleAuthProvider,
      firebase
  }