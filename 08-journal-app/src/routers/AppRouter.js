import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
import { AuthRouter } from './AuthRouter'
import { JournalScreen } from '../components/journal/JournalScreen'
import { firebase } from '../firebase/firebase-config'
import { useDispatch } from 'react-redux'
import { login } from '../actions/auth'
import { PublicRoute } from './PublicRoute'
import { PriveteRoute } from './PriveteRoute'
import {  startLoadingNotes } from '../actions/notes'
export const AppRouter = () => {
    
    const dispatch = useDispatch();
    const [checking, setchecking] = useState(true);
    const [loggedIn, setloggedIn] = useState(false);
    
    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user?.uid) {
                dispatch(login(user.uid, user.displayName));
                setloggedIn(true);
                dispatch(startLoadingNotes(user.uid));
            } else {
                setloggedIn(false);
            }
            setchecking(false);
        });
    }, [dispatch, setchecking, setloggedIn]);

    if (checking) {
        return (
            <div>Loading</div>
        );
    }
    return (
        <Router>
            <div>
                <Switch >
                    <PublicRoute isAuthenticated={loggedIn} path='/auth' component={AuthRouter} />
                    {/* <Route path='/auth' component={AuthRouter} /> */}
                    {/* <Route exact path='/' component={JournalScreen} /> */}
                    <PriveteRoute isAuthenticated={loggedIn} exact path='/' component={JournalScreen} />
                    <Redirect to='/auth/login' />
                </Switch>
            </div>
        </Router>
    )
}
