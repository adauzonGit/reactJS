import React from 'react';
import "@testing-library/jest-dom"
import { shallow } from 'enzyme';
import GifExpertApp from '../GifExpertApp';

describe('Pruebas sobre Gift Expert', () => {
    test('should Renderizar correctamente', () => {
        const wrapper = shallow(<GifExpertApp></GifExpertApp>);

        expect(wrapper).toMatchSnapshot();
    })
    test('should mostrar lista de categorias', () => {
        const categorias = ["One punch", "naruto"];
        const wrapper = shallow(<GifExpertApp defaultCategories={categorias}></GifExpertApp>)
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find("GifGrid").length).toBe(categorias.length);

    })

})
