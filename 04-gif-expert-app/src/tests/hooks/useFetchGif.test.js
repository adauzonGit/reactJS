import { useFetchGifs } from "../../hooks/useFetchGifs"
import { renderHook } from "@testing-library/react-hooks";



describe('Pruebas en customHook', () => {
    test('should retornar el estato inical', async () => {
        const category = "Dragon Ball";
        // const { data: images, loading } = useFetchGifs(category);
        const { result, waitForNextUpdate } = renderHook(() => useFetchGifs())
        await waitForNextUpdate();
        const { data, loading } = result.current;
        // console.log(data, loading);
        expect(data.length).toEqual(10);
        expect(loading).toEqual(false);
    })

    test('should retornar arreglo de imagenes y loading a false', async () => {
        const category = "Dragon Ball";
        // const { data: images, loading } = useFetchGifs(category);
        const { result, waitForNextUpdate } = renderHook(() => useFetchGifs())
        await waitForNextUpdate();
        const { data, loading } = result.current;
        expect(data.length).toEqual(10);
        expect(loading).toEqual(false);




    })

})
