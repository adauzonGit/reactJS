import React from 'react';
import "@testing-library/jest-dom"
import { shallow } from 'enzyme';
import { GifGrid } from '../../components/GifGrid';
import { useFetchGifs } from '../../hooks/useFetchGifs';

jest.mock('../../hooks/useFetchGifs');

describe('Pruebas sobre GifGrid', () => {

    const category = "One punc man";

    test('should Renderizar correctamente', () => {
        useFetchGifs.mockReturnValue({
            data: [],
            loading: true,
        });
        const wrapper = shallow(<GifGrid category={category}></GifGrid>);
        expect(wrapper).toMatchSnapshot();
    })

    test('should mostrar items cuando se carga imagenes useFetchGifs', () => {
        const gifs = [{
            id: "uu",
            url: "http",
            title: "example"
        }]
        useFetchGifs.mockReturnValue({
            data: gifs,
            loading: false
        })
        const wrapper = shallow(<GifGrid category={category}></GifGrid>);

        expect(wrapper.find("p").exists()).toBe(false);
        expect(wrapper.find("GifGridItem").exists()).toBe(true);
        expect(wrapper.find("GifGridItem").length).toBe(gifs.length);


    })


})
