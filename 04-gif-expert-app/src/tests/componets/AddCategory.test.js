import { shallow } from "enzyme"
import { AddCategory } from "../../components/AddCategory"
import React from 'react';
import '@testing-library/jest-dom';


describe('Pruebas en addCategory', () => {
    const setCategories = jest.fn();
    let wrapper = shallow(<AddCategory setCategories={setCategories}></AddCategory>);

    beforeEach(() => {
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategories={setCategories}></AddCategory>);
    })
    test('should Mostrarse correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    })
    test('should Cambios en caja de texto', () => {
        const input = wrapper.find("input");
        const value = "Hola mundo";
        input.simulate("change", { target: { value } });
        const p = wrapper.find("p");

        expect(p.text()).toBe(value);
    })
    test('should postear con submit vacio', () => {
        const form = wrapper.find("form");
        form.simulate("submit", {
            preventDefault() { }
        });

        expect(setCategories).not.toHaveBeenCalled();

    })

    test('should llamar el setCategories y limpiar caja de texto', () => {
        const input = wrapper.find("input");
        const value = "Hola mundo";
        input.simulate("change", { target: { value } });
        const form = wrapper.find("form");
        form.simulate("submit", {
            preventDefault() { }
        });
        // wrapper.update();
        expect(setCategories).toHaveBeenCalled();
        expect(setCategories).toHaveBeenCalledTimes(1);
        expect(setCategories).toHaveBeenCalledWith(expect.any(Function));
        expect(wrapper.find("input").prop("value")).toBe("");




    })


})



