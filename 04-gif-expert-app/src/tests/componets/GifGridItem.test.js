import { shallow } from "enzyme"
import { GifGridItem } from "../../components/GifGridItem"
import React from 'react';
import '@testing-library/jest-dom';

describe('Prueba de GifGridItem', () => {

    const title = 'Titulo 1';
    const url = 'http:s'
    const wrapper = shallow(<GifGridItem title={title} url={url}></GifGridItem>);


    test('should renderizar correctamente', () => {

        expect(wrapper).toMatchSnapshot();
    })

    test('should tener un parrafo con title', () => {
        const p = wrapper.find("p")
        expect(p.text()).toBe(title);
    })
    test('should imagen tener url y title', () => {
        const img = wrapper.find("img");
        expect(img.prop("src")).toBe(url)
        expect(img.prop("alt")).toBe(title)
    })

    test('should Tener clase', () => {
        const div = wrapper.find("div");    
        expect(true).toBe(div.hasClass("animate__fadeIn"));
    })


})
