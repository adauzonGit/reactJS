const { getGifs } = require("../../helpers/getGifs")



describe('Pruebas con getGifs fetch', () => {

    test('should traer 10 elementso', async () => {
        const gifs = await getGifs("One puch");
        expect(gifs.length).toBe(10);
    })
    test('should traer 0 elementso', async () => {
        const gifs = await getGifs("");
        console.log(gifs)
        expect(gifs.length).toBe(0);
    })

})
